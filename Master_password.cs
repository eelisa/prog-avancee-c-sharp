﻿using System;
using System.IO;
using System.Windows.Forms;
using Konscious.Security.Cryptography;
using System.Text;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Runtime.InteropServices;

namespace CoffreFort
{
    internal class Master_password : Password
    {
        // Attributs de la classe Master_password
        private byte[] pwd_hash;
        private byte[] salt_hash;

        // Accesseurs pwd_hash
        public byte[] Pwd_hash
        {
            get { return pwd_hash; }
            set { pwd_hash = value; }
        }

        // Accesseurs salt_hash
        public byte[] Salt_hash
        {
            get { return salt_hash; }
            set { salt_hash = value; }
        }

        // Constructeur
        public Master_password(byte[] pwd, byte[] hash)
        {
            pwd_hash = pwd;
            salt_hash = hash;
        }

        // Deuxième constructeur
        public Master_password(string str)
        {
            this.Pwd = str;
        }

        // Créer le salt du hash
        public void CreateSalt()
        {
            var buffer = new byte[16];
            var rng = new RNGCryptoServiceProvider();
            rng.GetBytes(buffer);
            this.Salt_hash = buffer;
        }

        // Hash une chaîne de caractères à partir de l'algorithme de hashage argon2id
        public byte[] HashPassword(string password)
        {
            var argon2 = new Argon2id(Encoding.UTF8.GetBytes(password));

            argon2.Salt = this.Salt_hash;
            argon2.DegreeOfParallelism = 8;
            argon2.Iterations = 2;
            argon2.MemorySize = 1024 * 256;

            return argon2.GetBytes(16);
        }

        // Fonction de comparaison des hash
        public bool VerifyHash(string password)
        {
            var newHash = HashPassword(password);
            return pwd_hash.SequenceEqual(newHash);
        }

        // Saisie du mot de passe maître à partir du formulaire
        public void entry(string pwd_entry)
        {
            Pwd = pwd_entry;
        }

        // Procédure de hashage du mot de passe maître
        public void procedure_hash_pwd()
        {
            this.Pwd_hash = HashPassword(Pwd);
        }

        // Fonction de chiffrement dans la mémoire RAM
        public byte[] EncryptFile(string password, byte[] inputFile)
        {
            byte[] keyByte = Encoding.UTF8.GetBytes(password);
            byte[] salt = new byte[] { 0x14, 0x00, 0x77, 0x55, 0x02, 0x04, 0x88, 0x99, 0x14, 0x50, 0x71, 0x55, 0x52, 0x04, 0x48, 0x00 };

            byte[] encryptedFile = null;

            try
            {
                using (var memory = new MemoryStream())
                {
                    using (var aes = new RijndaelManaged())
                    {
                        aes.Mode = CipherMode.CBC;
                        aes.Padding = PaddingMode.PKCS7;
                        aes.KeySize = 256;
                        aes.BlockSize = 128;

                        var key = new Rfc2898DeriveBytes(keyByte, salt, 2000);
                        aes.Key = key.GetBytes(aes.KeySize / 8);
                        aes.IV = key.GetBytes(aes.BlockSize / 8);

                        using (var cs = new CryptoStream(memory, aes.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(inputFile, 0, inputFile.Length);
                            cs.FlushFinalBlock();
                            cs.Close();
                        }
                    }
                    encryptedFile = memory.ToArray();
                    memory.Close();
                }

                return encryptedFile;

            }
            catch
            {
                return null;
            }

        }

        // Fonction de déchiffrement
        public byte[] DecryptFile(string password, byte[] inputFile)
        {
            byte[] keyByte = Encoding.UTF8.GetBytes(password);
            byte[] salt = new byte[] { 0x14, 0x00, 0x77, 0x55, 0x02, 0x04, 0x88, 0x99, 0x14, 0x50, 0x71, 0x55, 0x52, 0x04, 0x48, 0x00 };

            byte[] encryptedFile = null;

            try
            {
                using (var memory = new MemoryStream())
                {
                    using (var aes = new RijndaelManaged())
                    {
                        aes.Mode = CipherMode.CBC;
                        aes.Padding = PaddingMode.PKCS7;
                        aes.KeySize = 256;
                        aes.BlockSize = 128;

                        var key = new Rfc2898DeriveBytes(keyByte, salt, 2000);
                        aes.Key = key.GetBytes(aes.KeySize / 8);
                        aes.IV = key.GetBytes(aes.BlockSize / 8);

                        using (var cs = new CryptoStream(memory, aes.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(inputFile, 0, inputFile.Length);
                            cs.FlushFinalBlock();
                            cs.Close();
                        }
                    }
                    encryptedFile = memory.ToArray();
                    memory.Close();

                }

                return encryptedFile;


            }
            catch
            {
                return null;
            }

        }
    }
}
        