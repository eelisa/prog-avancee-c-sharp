﻿namespace CoffreFort
{
    partial class ConfirmationSauvegarde
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_message = new System.Windows.Forms.Label();
            this.bt_ok = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lb_message
            // 
            this.lb_message.AutoSize = true;
            this.lb_message.Location = new System.Drawing.Point(65, 43);
            this.lb_message.Name = "lb_message";
            this.lb_message.Size = new System.Drawing.Size(44, 16);
            this.lb_message.TabIndex = 0;
            this.lb_message.Text = "label1";
            // 
            // bt_ok
            // 
            this.bt_ok.Location = new System.Drawing.Point(208, 100);
            this.bt_ok.Name = "bt_ok";
            this.bt_ok.Size = new System.Drawing.Size(99, 25);
            this.bt_ok.TabIndex = 1;
            this.bt_ok.Text = "OK";
            this.bt_ok.UseVisualStyleBackColor = true;
            this.bt_ok.Click += new System.EventHandler(this.bt_ok_Click);
            // 
            // ConfirmationSauvegarde
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 173);
            this.Controls.Add(this.bt_ok);
            this.Controls.Add(this.lb_message);
            this.Name = "ConfirmationSauvegarde";
            this.Text = "ConfirmationSauvegarde";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfirmationSauvegarde_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button bt_ok;
        public System.Windows.Forms.Label lb_message;
    }
}