﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffreFort
{
    public partial class AuthentificationForte : Form
    {
        // Attributs de la classe AuthentificationForte
        public string code;
        public bool code_ok;
        
        // Constructeur
        public AuthentificationForte(string code_env)
        {
            InitializeComponent();
            code = code_env;
        }

        // Procédure au clic sur le bouton Valider
        private void bt_valid_Click(object sender, EventArgs e)
        {
            if (code == tb_code.Text)
            {
                code_ok = true;
                this.Hide();
            }
            else
            {
                code_ok = false;
                lb_error_code.Text = "Mauvais code de vérification";
                lb_error_code.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
}
