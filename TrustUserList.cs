﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffreFort
{
    internal class TrustUserList
    {
        // Attributs de la classe TrustUserList
        private List<TrustUser> trustuserlist;
        private int nb_tu;

        // Accesseurs de l'attribut trustuserlist
        public List<TrustUser> Trustuserlist
        {
        	get { return trustuserlist; }
        	set { trustuserlist = value; }
        }

        // Accesseurs de l'attribut nb_tu
        public int Nb_tu
        {
        	get { return nb_tu; }
        	set { nb_tu = value; }
        }

        // Constructeur par défault
        public TrustUserList()
        {
            trustuserlist = new List<TrustUser>();
        }
        
        // Deuxième constructeur
        public TrustUserList(int nb)
        {
            nb_tu = nb;
            trustuserlist = new List<TrustUser>(nb);
        }
    }
}
