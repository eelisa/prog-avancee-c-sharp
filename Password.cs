﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffreFort
{
    internal class Password
    {
        // Mot de passe 
        private string pwd;
        
        // Accesseurs de l'attribut pwd
        public string Pwd
        {
        	get { return pwd; }
        	set { pwd = value; }
        }
    }
}
