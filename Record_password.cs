﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffreFort
{
    internal class Record_password
    {
        // Tableau 2D des mots de passe et des utilisateurs de confiance
        public string[,] pwd_array;
        private int nb_rows;
        private int nb_cols;

        // Constructeur
        public Record_password(int nbr, int nbc)
        {
            nb_rows = nbr;
            nb_cols = nbc;
            pwd_array = new string[nb_rows, nb_cols];
        }

        // Accesseurs de l'attribut nb_rows
        public int Nb_rows
        {
        	get { return nb_rows; }
        	set { nb_rows = value; }
        }

        // Accesseurs de l'attribut nb_cols
        public int Nb_cols
        {
        	get { return nb_cols; }
        	set { nb_cols = value; }
        }
    }
}
