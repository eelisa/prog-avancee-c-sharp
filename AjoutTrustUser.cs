﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffreFort
{
    internal partial class AjoutTrustUser : Form
    {
        // Attribut la classe AjoutTrustUser
        public TrustUserList tul;

        // Constructeur
        public AjoutTrustUser()
        {
            InitializeComponent();
            tul = new TrustUserList(); 
        }

        // Procédure au clic sur le bouton Valider
        private void bt_val_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < lb_tu.Items.Count; i++)
            {
                TrustUser tu = new TrustUser(lb_tu.Items[i].ToString());
                tul.Trustuserlist.Add(tu);
            }
            this.DialogResult = DialogResult.OK;
            this.Hide();
        }

        // Ajout d'un utilisateur de confiance dans la listbox
        private void bt_add_Click(object sender, EventArgs e)
        {
            lb_tu.Items.Add(tb_user.Text);
            tb_user.Clear();
        }
    }
}
