﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffreFort
{
    public partial class ConfirmationSauvegarde : Form
    {
        // Constructeur
        public ConfirmationSauvegarde()
        {
            InitializeComponent();
        }

        // Procédure au clic sur bouton ok
        private void bt_ok_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        // Fermeture de la fenêtre 
        private void ConfirmationSauvegarde_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
        }
    }
}
