﻿namespace CoffreFort
{
    partial class CoffreFortOuvert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CoffreFortOuvert));
            this.dgv_pwdarray = new System.Windows.Forms.DataGridView();
            this.Titre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.e_mail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomUser = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.url = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.password = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsb_save = new System.Windows.Forms.ToolStripButton();
            this.tsb_add_pwd = new System.Windows.Forms.ToolStripButton();
            this.tb_create_open = new System.Windows.Forms.ToolStripButton();
            this.tsbt_add_tu = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_pwdarray)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_pwdarray
            // 
            this.dgv_pwdarray.AllowUserToAddRows = false;
            this.dgv_pwdarray.AllowUserToResizeColumns = false;
            this.dgv_pwdarray.AllowUserToResizeRows = false;
            this.dgv_pwdarray.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_pwdarray.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dgv_pwdarray.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_pwdarray.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Titre,
            this.e_mail,
            this.NomUser,
            this.url,
            this.password});
            this.dgv_pwdarray.Location = new System.Drawing.Point(0, 30);
            this.dgv_pwdarray.Name = "dgv_pwdarray";
            this.dgv_pwdarray.ReadOnly = true;
            this.dgv_pwdarray.RowHeadersWidth = 51;
            this.dgv_pwdarray.RowTemplate.Height = 24;
            this.dgv_pwdarray.Size = new System.Drawing.Size(932, 545);
            this.dgv_pwdarray.TabIndex = 0;
            this.dgv_pwdarray.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_pwdarray_EditingControlShowing);
            this.dgv_pwdarray.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_pwdarray_RowHeaderMouseDoubleClick);
            // 
            // Titre
            // 
            this.Titre.HeaderText = "Titre";
            this.Titre.MinimumWidth = 6;
            this.Titre.Name = "Titre";
            this.Titre.ReadOnly = true;
            // 
            // e_mail
            // 
            this.e_mail.HeaderText = "Email";
            this.e_mail.MinimumWidth = 6;
            this.e_mail.Name = "e_mail";
            this.e_mail.ReadOnly = true;
            // 
            // NomUser
            // 
            this.NomUser.HeaderText = "Nom d\'utilisateur";
            this.NomUser.MinimumWidth = 6;
            this.NomUser.Name = "NomUser";
            this.NomUser.ReadOnly = true;
            // 
            // url
            // 
            this.url.HeaderText = "URL";
            this.url.MinimumWidth = 6;
            this.url.Name = "url";
            this.url.ReadOnly = true;
            // 
            // password
            // 
            this.password.HeaderText = "Mot de passe";
            this.password.MinimumWidth = 6;
            this.password.Name = "password";
            this.password.ReadOnly = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tb_create_open,
            this.tsb_save,
            this.tsb_add_pwd,
            this.tsbt_add_tu});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(930, 27);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsb_save
            // 
            this.tsb_save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsb_save.Image = ((System.Drawing.Image)(resources.GetObject("tsb_save.Image")));
            this.tsb_save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_save.Name = "tsb_save";
            this.tsb_save.Size = new System.Drawing.Size(29, 24);
            this.tsb_save.Text = "Sauvegarder";
            this.tsb_save.Click += new System.EventHandler(this.tsb_save_Click);
            // 
            // tsb_add_pwd
            // 
            this.tsb_add_pwd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsb_add_pwd.Image = ((System.Drawing.Image)(resources.GetObject("tsb_add_pwd.Image")));
            this.tsb_add_pwd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_add_pwd.Name = "tsb_add_pwd";
            this.tsb_add_pwd.Size = new System.Drawing.Size(29, 24);
            this.tsb_add_pwd.Text = "Ajouter un mot de passe";
            this.tsb_add_pwd.Click += new System.EventHandler(this.tsb_add_pwd_Click);
            // 
            // tb_create_open
            // 
            this.tb_create_open.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tb_create_open.Image = ((System.Drawing.Image)(resources.GetObject("tb_create_open.Image")));
            this.tb_create_open.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tb_create_open.Name = "tb_create_open";
            this.tb_create_open.Size = new System.Drawing.Size(29, 24);
            this.tb_create_open.Text = "Créer ou ouvrir un coffre-fort";
            this.tb_create_open.Click += new System.EventHandler(this.tb_create_open_Click);
            // 
            // tsbt_add_tu
            // 
            this.tsbt_add_tu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbt_add_tu.Image = ((System.Drawing.Image)(resources.GetObject("tsbt_add_tu.Image")));
            this.tsbt_add_tu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbt_add_tu.Name = "tsbt_add_tu";
            this.tsbt_add_tu.Size = new System.Drawing.Size(29, 24);
            this.tsbt_add_tu.Text = "Ajouter un utilisateur de confiance";
            this.tsbt_add_tu.Click += new System.EventHandler(this.tsbt_add_tu_Click);
            // 
            // CoffreFortOuvert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(930, 575);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dgv_pwdarray);
            this.Name = "CoffreFortOuvert";
            this.Text = "CoffreFortOuvert";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CoffreFortOuvert_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_pwdarray)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsb_save;
        private System.Windows.Forms.ToolStripButton tsb_add_pwd;
        private System.Windows.Forms.ToolStripButton tb_create_open;
        private System.Windows.Forms.ToolStripButton tsbt_add_tu;
        public System.Windows.Forms.DataGridView dgv_pwdarray;
        private System.Windows.Forms.DataGridViewTextBoxColumn Titre;
        private System.Windows.Forms.DataGridViewTextBoxColumn e_mail;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomUser;
        private System.Windows.Forms.DataGridViewTextBoxColumn url;
        private System.Windows.Forms.DataGridViewTextBoxColumn password;
    }
}