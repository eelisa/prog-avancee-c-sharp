﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Threading.Tasks;

namespace CoffreFort
{
    internal class StrongBox
    {
        // Nom du coffre fort
        private string name;
        // Mot de passe maître
        private Master_password m_pwd;
        // Tableau de mot de passe
        public Record_password rpwd_array;
        // Tableau des propriétés des mots de passe
        public string [,] pwd_p_array;
        // Liste d'utilisateurs de confiance
        public TrustUserList trustusers;
        // Liste des mots de passe
        public List<string> password_list;

        // Accesseurs m_pwd
        public Master_password M_pwd
        {
            get { return m_pwd; }
            set { m_pwd = value; }
        }

        // Accesseurs name
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        // Constructeur pour la création d'une Strongbox à partir d'un fichier
        public StrongBox(int t_rpwda)
        {
            trustusers = new TrustUserList();
            password_list = new List<string>();
            pwd_p_array= new string[t_rpwda, 4];
        }

        // Constructeur pour la création d'une Strongbox à partir du formulaire CoffreFortOuvert
        public StrongBox(string na, Master_password mpwd, TrustUserList tu_l)
        {
            this.name = na;
            this.m_pwd = mpwd;
            this.trustusers = tu_l;
            password_list = new List<string>();
        }

        // Création du tableau de mots de passe et de trustusers 
        public void CreateRecordPasswordArray(CoffreFortOuvert cf_open)
        {
            int nbr = cf_open.dgv_pwdarray.RowCount;
            int nbc = cf_open.dgv_pwdarray.ColumnCount;
            if (nbr < trustusers.Trustuserlist.Count) rpwd_array = new Record_password(trustusers.Trustuserlist.Count, nbc + 1);
            else rpwd_array = new Record_password(nbr,nbc+1);
            
            // Ajout des propriétés des mots de passe dans le tableau
            for (int i = 0; i < nbr; i++)
            {
                for (int j = 0; j < nbc-1; j++)
                {
                    rpwd_array.pwd_array[i,j] = cf_open.dgv_pwdarray.Rows[i].Cells[j].Value.ToString();
                }
            }
            // Ajout des mots de passe dans le tableau
            for (int i = 0; i < nbr; i++)
            {
                rpwd_array.pwd_array[i, 4] = password_list[i];
            }
            // Ajout des trustusers dans la dernière colonne du tableau
            for (int i = 0; i < trustusers.Trustuserlist.Count; i++)
            {
                rpwd_array.pwd_array[i,5] = trustusers.Trustuserlist[i].Email;
            }
            
        }

        // Création du répertoire et des fichiers textes, enfin chiffrement du fichier avec les mots de passe
        public void CreateTextFile()
        {
            // Création des répertoires
            string dir_principal = @"c:\CoffreFort";
            if (!Directory.Exists(dir_principal)) Directory.CreateDirectory(dir_principal);
            string sub_folder = @"c:\CoffreFort\\"+this.name;
            if (Directory.Exists (sub_folder)) Directory.Delete (sub_folder, true);
            Directory.CreateDirectory (sub_folder);
            // Création du fichier pour les mots de passe et utilisateurs de confiance
            string path = @"c:\CoffreFort\"+this.name+"\\"+this.name+".txt";
            using (StreamWriter sw = File.CreateText(path))
            {
                for (int i = 0; i < rpwd_array.Nb_rows; i++)
                {
                    for (int j = 0; j < rpwd_array.Nb_cols; j++)
                    {
                        sw.Write(rpwd_array.pwd_array[i, j] + "•");
                    }
                    sw.Write("\n");
                }
            }
            // Chiffrement du fichier pour les mots de passe et utilisateurs de confiance
            byte[] data = File.ReadAllBytes (path);
            var file = m_pwd.EncryptFile(m_pwd.Pwd, data);
            File.WriteAllBytes (path, file);
            // Création du fichier pour le hash du mot de passe maître et le salt utilisé pour le chiffrement
            string hash_path = @"c:\CoffreFort\" + this.name + "\\" + this.name + "_hash.txt";
            m_pwd.CreateSalt();
            m_pwd.procedure_hash_pwd();
            string hash_str = Convert.ToBase64String(m_pwd.Pwd_hash);
            string salt_str = Convert.ToBase64String(m_pwd.Salt_hash);
            using (StreamWriter sw_hash = File.CreateText(hash_path))
            {
                sw_hash.WriteLine(hash_str);
                sw_hash.WriteLine(salt_str);
            }
        }

        // Sauvegarde du coffre-fort
        public void SaveStrongBox(CoffreFortOuvert cfopen)
        {
            if (!password_list.Any())
            {
                CoffreFortVide cfv = new CoffreFortVide();
                cfv.ShowDialog();
            }
            else
            {
                this.CreateRecordPasswordArray(cfopen);
                CreateTextFile();
                ConfirmationSauvegarde c_save = new ConfirmationSauvegarde();
                c_save.lb_message.Text = "Votre Coffre-fort a bien été enregistré ici : C:\\CoffreFort\\ " + this.name;
                c_save.Show();
            }
        }
    }
}
