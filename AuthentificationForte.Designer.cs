﻿namespace CoffreFort
{
    partial class AuthentificationForte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthentificationForte));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_code = new System.Windows.Forms.TextBox();
            this.bt_valid = new System.Windows.Forms.Button();
            this.lb_error_code = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(50, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(322, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Un code de vérification vous a été envoyé par email. ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(50, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Entrer le code :";
            // 
            // tb_code
            // 
            this.tb_code.Location = new System.Drawing.Point(170, 71);
            this.tb_code.Name = "tb_code";
            this.tb_code.Size = new System.Drawing.Size(202, 22);
            this.tb_code.TabIndex = 2;
            // 
            // bt_valid
            // 
            this.bt_valid.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bt_valid.Location = new System.Drawing.Point(127, 132);
            this.bt_valid.Name = "bt_valid";
            this.bt_valid.Size = new System.Drawing.Size(147, 23);
            this.bt_valid.TabIndex = 3;
            this.bt_valid.Text = "Valider";
            this.bt_valid.UseVisualStyleBackColor = false;
            this.bt_valid.Click += new System.EventHandler(this.bt_valid_Click);
            // 
            // lb_error_code
            // 
            this.lb_error_code.AutoSize = true;
            this.lb_error_code.Location = new System.Drawing.Point(167, 96);
            this.lb_error_code.Name = "lb_error_code";
            this.lb_error_code.Size = new System.Drawing.Size(0, 16);
            this.lb_error_code.TabIndex = 4;
            // 
            // AuthentificationForte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(739, 419);
            this.Controls.Add(this.lb_error_code);
            this.Controls.Add(this.bt_valid);
            this.Controls.Add(this.tb_code);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AuthentificationForte";
            this.Text = "AuthentificationForte";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_code;
        private System.Windows.Forms.Button bt_valid;
        public System.Windows.Forms.Label lb_error_code;
    }
}