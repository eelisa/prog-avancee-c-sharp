﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net.Mail;

namespace CoffreFort
{
    internal partial class Ouverture : Form
    {
        // Attributs de la classe Ouverture
        public string filename;
        public StrongBox sb_file;

        // Constructeur
        public Ouverture(string fname)
        {
            InitializeComponent();
            filename = fname;
        }

        // Procédure exécutée au clic sur le bouton valider pour ouvrir un Coffre-fort
        private void bt_valider_Click(object sender, EventArgs e)
        {
            lb_error_pwd.Text = "";
            // Récupération du hash du mot de passe et du salt
            string sb_name= filename.Split('\\')[2];
            string path_file_hash = @"c:\CoffreFort\" + sb_name + "\\" + sb_name + "_hash.txt";
            string path_file_pwd = @"c:\CoffreFort\" + sb_name + "\\" + sb_name + ".txt";
            List<string> list_hash = new List<string>();
            foreach (string line in File.ReadLines(path_file_hash))
            {
                list_hash.Add(line);
            }
            // Conversion des chaines de caractères en base64 vers des tableaux d'octets
            byte[] bytes_pwd = Convert.FromBase64String(list_hash[0]);
            byte[] bytes_salt = Convert.FromBase64String(list_hash[1]);

            // Construction d'un master_password avec les informations récupérées
            Master_password mp = new Master_password(bytes_pwd, bytes_salt);
            mp.Pwd = tb_mpwd.Text;
            bool result = (tb_mpwd.Text != "" && mp.VerifyHash(tb_mpwd.Text));
            
            // Si la clé est bonne, c'est à dire si le mot de passe maître entré est le bon : déchiffrement du fichier
            if (result)
            {
                // Déchiffrement du fichier 
                byte[] bytes_file_pwd = File.ReadAllBytes(path_file_pwd);
                var file = mp.DecryptFile(tb_mpwd.Text, bytes_file_pwd);
                File.WriteAllBytes(path_file_pwd, file);

                // Récupération des informations
                string[] lines = File.ReadAllLines(path_file_pwd);
                int nb_lines = 0;
                foreach (string line in lines) nb_lines++;
                sb_file = new StrongBox(nb_lines);
                sb_file.M_pwd = mp;
                sb_file.Name = sb_name;
                int nbr = 0;
                foreach (string line in lines)
                {
                    // Récupération des utilisateurs de confiance
                    string value_tu = line.Split('•')[5];
                    TrustUser tu = new TrustUser(value_tu);
                    sb_file.trustusers.Trustuserlist.Add(tu);  
                    
                    // Récupération des mots de passe
                    string value_pwd = line.Split('•')[4];
                    sb_file.password_list.Add(value_pwd);

                    // Récupération des propriétés des mots de passe
                    for (int i = 0; i < 4; i++)
                    {
                        sb_file.pwd_p_array[nbr, i] = line.Split('•')[i];
                    }
                    nbr++;
                }

                // Rechiffrement du fichier 
                byte[] bytes_file_pwd_encr = File.ReadAllBytes(path_file_pwd);
                var file_enc = mp.EncryptFile(tb_mpwd.Text, bytes_file_pwd_encr);
                File.WriteAllBytes(path_file_pwd, file_enc);

                // Vérification que le trustuser appartient à la liste des utilisateurs de confiance associé au Coffre-fort
                bool test_user = false;
                for (int i = 0; i < nb_lines; i++)
                {
                    if (tb_trustu.Text == sb_file.trustusers.Trustuserlist[i].Email && tb_trustu.Text.Length != 0) test_user = true;
                }
                if (test_user)
                {
                    // Envoi du code de vérification par mail
                    MailMessage mail = new MailMessage("CoffreFort.progAvancee@gmail.com", tb_trustu.Text);
                    mail.Subject = "Code vérification CoffreFort";
                    Random rand = new Random();
                    string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    string code = new string(Enumerable.Repeat(chars, 8).Select(s => s[rand.Next(s.Length)]).ToArray());
                    mail.Body = "Code de vérification : " + code;

                    SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);

                    smtpClient.Credentials = new System.Net.NetworkCredential()
                    {
                        UserName = "CoffreFort.progAvancee@gmail.com",
                        Password = "CoffreFort.progAvancee!9"
                    };

                    smtpClient.EnableSsl = true;
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s,
                            System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                            System.Security.Cryptography.X509Certificates.X509Chain chain,
                            System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };

                    smtpClient.Send(mail);
                    
                    // Authentification forte : vérification que le code entré est le même que le code qui a été envoyé
                    AuthentificationForte af = new AuthentificationForte(code);
                    af.ShowDialog();

                    if (af.code_ok)
                    {
                        CoffreFortOuvert cfopen = new CoffreFortOuvert(sb_file);
                        for (int i = 0; i < nb_lines; i++)
                        {
                            cfopen.dgv_pwdarray.Rows.Add(sb_file.pwd_p_array[i, 0], sb_file.pwd_p_array[i, 1], sb_file.pwd_p_array[i, 2], sb_file.pwd_p_array[i, 3], "••••••••••");
                        }
                        cfopen.Show();
                        this.Hide();
                    }

                }
                else
                {
                    lb_error_tu.Text = "L'utilisateur entré ne fait pas partie de la liste des utilisateurs de confiance";
                    lb_error_tu.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                lb_error_pwd.Text = "Le mot de passe entré est incorrect";
                lb_error_pwd.ForeColor = System.Drawing.Color.Red;
            }
        }

        // Fermeture de l'application
        private void Ouverture_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        // Bouton permettant de revenir à la page home
        private void bt_back_Click(object sender, EventArgs e)
        {
            CoffreFort cf = new CoffreFort();
            this.Hide();
            cf.Show();
        }

        // Checkbox permettant d'afficher le mot de passe en clair
        private void cb_pwd_CheckedChanged(object sender, EventArgs e)
        {
            tb_mpwd.PasswordChar = cb_pwd.Checked ? '\0' : '•';
        }
    }
}
