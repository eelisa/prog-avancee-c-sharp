﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffreFort
{
    internal partial class CoffreFortOuvert : Form
    {
        // Attributs de la classe CoffreFortOuvert
        public StrongBox sbox;

        // Constructeur
        public CoffreFortOuvert()
        {

        }

        // Deuxième constructeur
        public CoffreFortOuvert(StrongBox sb)
        {
            InitializeComponent();
            sbox = sb;
            this.Text = sbox.Name;
        }

        // Fermeture de l'application entière
        private void CoffreFortOuvert_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        // Procédure au clic sur le bouton Ajouter un mot de passe
        private void tsb_add_pwd_Click(object sender, EventArgs e)
        {
            AjoutMotDePasse add_pwd = new AjoutMotDePasse(this);
            add_pwd.Show();
        }

        // Procédure au clic sur le bouton sauvegarder un Coffre-fort
        private void tsb_save_Click(object sender, EventArgs e)
        {
            sbox.SaveStrongBox(this);
        }

        //  Procédure au double-clic sur un mot de passe du tableau
        private void dgv_pwdarray_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            EditionMotDePasse ed_pwd = new EditionMotDePasse(this,e.RowIndex);
            ed_pwd.tb_title.Text = dgv_pwdarray.Rows[e.RowIndex].Cells[0].Value.ToString();
            ed_pwd.tb_email.Text = dgv_pwdarray.Rows[e.RowIndex].Cells[1].Value.ToString();
            ed_pwd.tb_login.Text = dgv_pwdarray.Rows[e.RowIndex].Cells[2].Value.ToString();
            ed_pwd.tb_url.Text = dgv_pwdarray.Rows[e.RowIndex].Cells[3].Value.ToString();
            ed_pwd.tb_pwd.Text = sbox.password_list[e.RowIndex];
            ed_pwd.Show();
        }

        // Procédure pour cacher le mot de passe
        private void dgv_pwdarray_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgv_pwdarray.CurrentCell.ColumnIndex == 3)
            {
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.UseSystemPasswordChar = true;
                }
            }
        }

        // Procédure au clic sur le bouton Créer ou Ouvrir un Coffre-fort
        private void tb_create_open_Click(object sender, EventArgs e)
        {
            CoffreFort cf = new CoffreFort();
            cf.Show();
        }

        // Procédure au clic sur le bouton Ajouter un utilisateur de confiance
        private void tsbt_add_tu_Click(object sender, EventArgs e)
        {
            AjoutTrustUser atu = new AjoutTrustUser();
            var result = atu.ShowDialog();
            if (result == DialogResult.OK)
            {
                for (int i = 0; i < atu.tul.Trustuserlist.Count; i++)
                {
                    sbox.trustusers.Trustuserlist.Add(atu.tul.Trustuserlist[i]);
                }
            }
        }
    }
}
