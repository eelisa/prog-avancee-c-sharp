namespace CoffreFort
{
    partial class AjoutMotDePasse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_edit = new System.Windows.Forms.Button();
            this.tb_title = new System.Windows.Forms.TextBox();
            this.tb_login = new System.Windows.Forms.TextBox();
            this.tb_pwd = new System.Windows.Forms.TextBox();
            this.tb_url = new System.Windows.Forms.TextBox();
            this.tb_email = new System.Windows.Forms.TextBox();
            this.lb_title = new System.Windows.Forms.Label();
            this.lb_email = new System.Windows.Forms.Label();
            this.lb_login = new System.Windows.Forms.Label();
            this.lb_pwd = new System.Windows.Forms.Label();
            this.lb_url = new System.Windows.Forms.Label();
            this.cb_sh_pwd = new System.Windows.Forms.CheckBox();
            this.bt_generate = new System.Windows.Forms.Button();
            this.pwd_lvl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bt_edit
            // 
            this.bt_edit.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bt_edit.Location = new System.Drawing.Point(251, 386);
            this.bt_edit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_edit.Name = "bt_edit";
            this.bt_edit.Size = new System.Drawing.Size(177, 34);
            this.bt_edit.TabIndex = 6;
            this.bt_edit.Text = "Ajouter";
            this.bt_edit.UseVisualStyleBackColor = false;
            this.bt_edit.Click += new System.EventHandler(this.bt_edit_Click);
            // 
            // tb_title
            // 
            this.tb_title.Location = new System.Drawing.Point(182, 52);
            this.tb_title.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_title.Name = "tb_title";
            this.tb_title.Size = new System.Drawing.Size(332, 22);
            this.tb_title.TabIndex = 1;
            // 
            // tb_login
            // 
            this.tb_login.Location = new System.Drawing.Point(182, 177);
            this.tb_login.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_login.Name = "tb_login";
            this.tb_login.Size = new System.Drawing.Size(332, 22);
            this.tb_login.TabIndex = 3;
            // 
            // tb_pwd
            // 
            this.tb_pwd.Location = new System.Drawing.Point(182, 315);
            this.tb_pwd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_pwd.Name = "tb_pwd";
            this.tb_pwd.PasswordChar = '•';
            this.tb_pwd.Size = new System.Drawing.Size(332, 22);
            this.tb_pwd.TabIndex = 5;
            this.tb_pwd.TextChanged += new System.EventHandler(this.tb_pwd_TextChanged);
            // 
            // tb_url
            // 
            this.tb_url.Location = new System.Drawing.Point(182, 245);
            this.tb_url.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_url.Name = "tb_url";
            this.tb_url.Size = new System.Drawing.Size(332, 22);
            this.tb_url.TabIndex = 4;
            // 
            // tb_email
            // 
            this.tb_email.Location = new System.Drawing.Point(182, 112);
            this.tb_email.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_email.Name = "tb_email";
            this.tb_email.Size = new System.Drawing.Size(332, 22);
            this.tb_email.TabIndex = 2;
            // 
            // lb_title
            // 
            this.lb_title.AutoSize = true;
            this.lb_title.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lb_title.Location = new System.Drawing.Point(37, 52);
            this.lb_title.Name = "lb_title";
            this.lb_title.Size = new System.Drawing.Size(40, 16);
            this.lb_title.TabIndex = 6;
            this.lb_title.Text = "Titre :";
            // 
            // lb_email
            // 
            this.lb_email.AutoSize = true;
            this.lb_email.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lb_email.Location = new System.Drawing.Point(37, 112);
            this.lb_email.Name = "lb_email";
            this.lb_email.Size = new System.Drawing.Size(50, 16);
            this.lb_email.TabIndex = 7;
            this.lb_email.Text = "Email : ";
            // 
            // lb_login
            // 
            this.lb_login.AutoSize = true;
            this.lb_login.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lb_login.Location = new System.Drawing.Point(37, 177);
            this.lb_login.Name = "lb_login";
            this.lb_login.Size = new System.Drawing.Size(46, 16);
            this.lb_login.TabIndex = 8;
            this.lb_login.Text = "Login :";
            // 
            // lb_pwd
            // 
            this.lb_pwd.AutoSize = true;
            this.lb_pwd.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lb_pwd.Location = new System.Drawing.Point(37, 318);
            this.lb_pwd.Name = "lb_pwd";
            this.lb_pwd.Size = new System.Drawing.Size(95, 16);
            this.lb_pwd.TabIndex = 9;
            this.lb_pwd.Text = "Mot de passe :";
            // 
            // lb_url
            // 
            this.lb_url.AutoSize = true;
            this.lb_url.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lb_url.Location = new System.Drawing.Point(37, 245);
            this.lb_url.Name = "lb_url";
            this.lb_url.Size = new System.Drawing.Size(40, 16);
            this.lb_url.TabIndex = 10;
            this.lb_url.Text = "URL :";
            // 
            // cb_sh_pwd
            // 
            this.cb_sh_pwd.AutoSize = true;
            this.cb_sh_pwd.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.cb_sh_pwd.Location = new System.Drawing.Point(564, 310);
            this.cb_sh_pwd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_sh_pwd.Name = "cb_sh_pwd";
            this.cb_sh_pwd.Size = new System.Drawing.Size(158, 20);
            this.cb_sh_pwd.TabIndex = 11;
            this.cb_sh_pwd.Text = "Afficher mot de passe";
            this.cb_sh_pwd.UseVisualStyleBackColor = true;
            this.cb_sh_pwd.CheckedChanged += new System.EventHandler(this.cb_sh_pwd_CheckedChanged);
            // 
            // bt_generate
            // 
            this.bt_generate.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bt_generate.Location = new System.Drawing.Point(554, 337);
            this.bt_generate.Margin = new System.Windows.Forms.Padding(5);
            this.bt_generate.Name = "bt_generate";
            this.bt_generate.Size = new System.Drawing.Size(179, 28);
            this.bt_generate.TabIndex = 12;
            this.bt_generate.Text = "Générer un mot de passe";
            this.bt_generate.UseVisualStyleBackColor = false;
            this.bt_generate.Click += new System.EventHandler(this.bt_generate_Click);
            // 
            // pwd_lvl
            // 
            this.pwd_lvl.AutoSize = true;
            this.pwd_lvl.Location = new System.Drawing.Point(179, 343);
            this.pwd_lvl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.pwd_lvl.Name = "pwd_lvl";
            this.pwd_lvl.Size = new System.Drawing.Size(0, 16);
            this.pwd_lvl.TabIndex = 13;
            // 
            // AjoutMotDePasse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(804, 466);
            this.Controls.Add(this.pwd_lvl);
            this.Controls.Add(this.bt_generate);
            this.Controls.Add(this.cb_sh_pwd);
            this.Controls.Add(this.lb_url);
            this.Controls.Add(this.lb_pwd);
            this.Controls.Add(this.lb_login);
            this.Controls.Add(this.lb_email);
            this.Controls.Add(this.lb_title);
            this.Controls.Add(this.tb_email);
            this.Controls.Add(this.tb_url);
            this.Controls.Add(this.tb_pwd);
            this.Controls.Add(this.tb_login);
            this.Controls.Add(this.tb_title);
            this.Controls.Add(this.bt_edit);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "AjoutMotDePasse";
            this.Text = "AjoutMotDePasse";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AjoutMotDePasse_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_edit;
        public System.Windows.Forms.Label lb_title;
        private System.Windows.Forms.Label lb_email;
        private System.Windows.Forms.Label lb_login;
        private System.Windows.Forms.Label lb_pwd;
        private System.Windows.Forms.Label lb_url;
        public System.Windows.Forms.TextBox tb_title;
        public System.Windows.Forms.TextBox tb_login;
        public System.Windows.Forms.TextBox tb_pwd;
        public System.Windows.Forms.TextBox tb_url;
        public System.Windows.Forms.TextBox tb_email;
        private System.Windows.Forms.CheckBox cb_sh_pwd;
        private System.Windows.Forms.Button bt_generate;
        private System.Windows.Forms.Label pwd_lvl;
    }
}