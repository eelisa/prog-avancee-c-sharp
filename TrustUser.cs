﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffreFort
{
    internal class TrustUser
    {
        // Attributs de la classe TrustUser
        private string email;

        // Accesseurs de l'attribut email
        [DisplayName("Adresse mail")]
        public string Email
        {
        	get { return email; }
        	set { email = value; }
        }

        // Constructeur
        public TrustUser(string mail)
        {
            email = mail;
        }
    }
}
