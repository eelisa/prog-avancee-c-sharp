﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace CoffreFort
{
    public partial class CreationCoffreFort : Form
    {
        // Constructeur
        public CreationCoffreFort()
        {
            InitializeComponent();
        }

        // Fermeture de l'application entière
        private void CreationCoffreFort_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        // Fonctionnalité du bouton "Ajouter"
        private void bt_ajouter_Click(object sender, EventArgs e)
        {
            lb_trustusers.Items.Add(tb_user.Text);
            tb_user.Clear();
        }

        // Fonctionnalité du bouton "Créer"
        private void bt_creer_Click(object sender, EventArgs e)
        {
            lb_name_m.Text = "";
            lb_pwd_m.Text = "";
            lb_tu_m.Text = "";
            // Permet de vérifier la force du mot de passe 
            if (tb_name.Text.Length == 0)
            {
                lb_name_m.Text = "Une valeur doit être entrée pour le nom du Coffre-fort";
                lb_name_m.ForeColor = System.Drawing.Color.Red;
            }
            else if (tb_master_pwd.Text.Length == 0)
            {
                lb_pwd_m.Text = "Un mot de passe maître doit être entré";
                lb_pwd_m.ForeColor = System.Drawing.Color.Red;
            }
            else if (lb_trustusers.Items.Count == 0)
            {
                lb_tu_m.Text = "Un Coffre-fort doit avoir au moins un utilisateur de confiance";
                lb_tu_m.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                string name = tb_name.Text;
                string test_path = @"c:\CoffreFort\" + name;
                if (Directory.Exists(test_path))
                {
                    //Créer une nouvelle fenêtre qui dit que le coffre fort de ce nom existe déjà
                    NomExistant name_e = new NomExistant();
                    name_e.lb_message.Text = "Un Coffre Fort nommé " + name + " existe déjà, entrez une nouvelle valeur";
                    name_e.Show();
                }
                else
                {
                    // Créer un Coffre-fort à partir des informations entrées
                    string str_mpwd = tb_master_pwd.Text;
                    Master_password m_pwd = new Master_password(str_mpwd);
                    TrustUserList tu_list = new TrustUserList();
                    for (int i = 0; i < lb_trustusers.Items.Count; i++)
                    {
                        TrustUser tu = new TrustUser(lb_trustusers.Items[i].ToString());
                        tu_list.Trustuserlist.Add(tu);
                    }
                    StrongBox sb = new StrongBox(name, m_pwd, tu_list);
                    CoffreFortOuvert cfo = new CoffreFortOuvert(sb);
                    this.Hide();
                    cfo.Show();
                }
            }   
        }

        // Permet d'afficher le mot de passe en clair
        private void cb_sh_pwd_CheckedChanged(object sender, EventArgs e)
        {
            tb_master_pwd.PasswordChar = cb_sh_pwd.Checked ? '\0' : '•';
        }

        // Permet de revenir à l'accueil
        private void bt_back_Click(object sender, EventArgs e)
        {
            CoffreFort cf = new CoffreFort();
            this.Hide();
            cf.Show();
        }

        // Permet de générer un mot de passe fort
        private void bt_generate_Click(object sender, EventArgs e)
        {
            const string cs = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLM?OPQRSTUVWXYZ&(_@/),;.?][#*$^%";

            StringBuilder sb = new StringBuilder();
            Random rand = new Random();
            for (int i = 0; i < 16; i++)
            {
                int index = rand.Next(cs.Length);
                sb.Append(cs[index]);

            }

            tb_master_pwd.Text = sb.ToString();
        }

        // Permet de vérifier la force du mot de passe maître 
        private void tb_master_pwd_TextChanged(object sender, EventArgs e)
        {
            string password = tb_master_pwd.Text;

            if (string.IsNullOrEmpty(password))
                pwd_lvl.Text = "";

            if (password.Length >= 1 && password.Length < 4)
            {
                pwd_lvl.ForeColor = System.Drawing.Color.Red;
                pwd_lvl.Text = "Mot de passe très faible";
            }

            if (password.Length >= 8)
            {
                pwd_lvl.ForeColor = System.Drawing.Color.Orange;
                pwd_lvl.Text = "Mot de passe moyen";
            }

            if (password.Length >= 12)
            {
                if (Regex.IsMatch(password, @"[0-9]+(\.[0-9][0-9]?)?", RegexOptions.ECMAScript))   //contient que des chiffres
                {
                    pwd_lvl.ForeColor = System.Drawing.Color.Orange;
                    pwd_lvl.Text = "Mot de passe moyen";
                }
                if (Regex.IsMatch(password, @"^(?=.*[a-z])(?=.*[A-Z]).+$", RegexOptions.ECMAScript)) //Majuscule et minuscule
                {
                    pwd_lvl.ForeColor = System.Drawing.Color.LimeGreen;
                    pwd_lvl.Text = "Mot de passe fort";
                }
                if (Regex.IsMatch(password, @"[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]", RegexOptions.ECMAScript)) //Contient les caractères spéciaux 
                {
                    pwd_lvl.ForeColor = System.Drawing.Color.Green;
                    pwd_lvl.Text = "Mot de passe trop fort";
                }
            }
        }
    }
}
