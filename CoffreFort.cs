﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CoffreFort
{
    internal partial class CoffreFort : Form
    {
        // Constructeur 
        public CoffreFort()
        {
            InitializeComponent();
        }

        // Procédure au clic sur le bouton Créer un coffre-fort
        private void bt_create_Click(object sender, EventArgs e)
        {
            CreationCoffreFort cr_SB = new CreationCoffreFort();
            this.Hide();
            cr_SB.Show();
        }

        // Procédure au clic sur le bouton Ouvrir un coffre-fort
        private void bt_open_Click(object sender, EventArgs e)
        {
            OpenFileDialog ovd = new OpenFileDialog();
            ovd.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (ovd.ShowDialog() == DialogResult.OK)
            {
                Ouverture open = new Ouverture(ovd.FileName);
                open.Show();
                this.Hide();
            };
        }

        // Fermeture de l'application
        private void CoffreFort_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
