﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace CoffreFort
{
    internal partial class AjoutMotDePasse : Form
    {
        // Attributs de la classe AjoutMotDePasse
        public string title;
        public string email;
        public string login;
        public string url;
        public CoffreFortOuvert cf_open;

        // Constructeur 
        public AjoutMotDePasse(CoffreFortOuvert cfo)
        {
            InitializeComponent();
            cf_open = cfo;
        }

        // Procédure au clic sur le bouton Éditer mot de passe
        private void bt_edit_Click(object sender, EventArgs e)
        {
            this.title = tb_title.Text;
            this.email = tb_email.Text;
            this.login = tb_login.Text;
            this.url = tb_url.Text;
            cf_open.sbox.password_list.Add(tb_pwd.Text);
            string[] arr_pwd = new string[5] { title, email, login, url, "••••••••••" };
            cf_open.dgv_pwdarray.Rows.Add(arr_pwd);
            this.Hide();
        }

        // Fermeture de la fenêtre
        private void AjoutMotDePasse_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
        }

        // Permet d'afficher le mot de passe en clair
        private void cb_sh_pwd_CheckedChanged(object sender, EventArgs e)
        {
            tb_pwd.PasswordChar = cb_sh_pwd.Checked ? '\0' : '•';
        }

        // Génération d'un mot de passe fort à 16 caractères
        private void bt_generate_Click(object sender, EventArgs e)
        {
            const string cs = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLM?OPQRSTUVWXYZ&(_@/),;.?][#*$^%";

            StringBuilder sb = new StringBuilder();
            Random rand = new Random();
            for (int i = 0; i < 16; i++)
            {
                int index = rand.Next(cs.Length);
                sb.Append(cs[index]);

            }

            tb_pwd.Text = sb.ToString();
        }

        // Vérification politique de mot de passe
        private void tb_pwd_TextChanged(object sender, EventArgs e)
        {
            string password = tb_pwd.Text;

            if (string.IsNullOrEmpty(password))
                pwd_lvl.Text = "";

            if (password.Length >= 1 && password.Length < 4)
            {
                pwd_lvl.ForeColor = System.Drawing.Color.Red;
                pwd_lvl.Text = "Mot de passe très faible";
            }

            if (password.Length >= 8)
            {
                pwd_lvl.ForeColor = System.Drawing.Color.Orange;
                pwd_lvl.Text = "Mot de passe moyen";
            }

            if (password.Length >= 12)
            {
                if (Regex.IsMatch(password, @"[0-9]+(\.[0-9][0-9]?)?", RegexOptions.ECMAScript))   //contient que des chiffres
                {
                    pwd_lvl.ForeColor = System.Drawing.Color.Orange;
                    pwd_lvl.Text = "Mot de passe moyen";
                }
                if (Regex.IsMatch(password, @"^(?=.*[a-z])(?=.*[A-Z]).+$", RegexOptions.ECMAScript)) //Majuscule et minuscule
                {
                    pwd_lvl.ForeColor = System.Drawing.Color.LimeGreen;
                    pwd_lvl.Text = "Mot de passe fort";
                }
                if (Regex.IsMatch(password, @"[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]", RegexOptions.ECMAScript)) //Contient les caractères spéciaux 
                {
                    pwd_lvl.ForeColor = System.Drawing.Color.Green;
                    pwd_lvl.Text = "Mot de passe trop fort";
                }
            }
        
         }
    }
}
