namespace CoffreFort
{
    partial class CreationCoffreFort
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.tb_master_pwd = new System.Windows.Forms.TextBox();
            this.lb_trustusers = new System.Windows.Forms.ListBox();
            this.tb_user = new System.Windows.Forms.TextBox();
            this.bt_ajouter = new System.Windows.Forms.Button();
            this.bt_creer = new System.Windows.Forms.Button();
            this.bt_back = new System.Windows.Forms.Button();
            this.cb_sh_pwd = new System.Windows.Forms.CheckBox();
            this.lb_pwd_m = new System.Windows.Forms.Label();
            this.lb_tu_m = new System.Windows.Forms.Label();
            this.lb_name_m = new System.Windows.Forms.Label();
            this.bt_generate = new System.Windows.Forms.Button();
            this.pwd_lvl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(48, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(449, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pour créer un nouveau coffre-fort, veuillez entrer les informations suivantes :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(48, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nom du coffre-fort : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(48, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Mot de passe maître :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(48, 204);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(213, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Liste des utilisateurs de confiance :";
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(317, 65);
            this.tb_name.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(295, 22);
            this.tb_name.TabIndex = 4;
            // 
            // tb_master_pwd
            // 
            this.tb_master_pwd.Location = new System.Drawing.Point(317, 126);
            this.tb_master_pwd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_master_pwd.Name = "tb_master_pwd";
            this.tb_master_pwd.PasswordChar = '•';
            this.tb_master_pwd.Size = new System.Drawing.Size(295, 22);
            this.tb_master_pwd.TabIndex = 5;
            this.tb_master_pwd.TextChanged += new System.EventHandler(this.tb_master_pwd_TextChanged);
            // 
            // lb_trustusers
            // 
            this.lb_trustusers.FormattingEnabled = true;
            this.lb_trustusers.ItemHeight = 16;
            this.lb_trustusers.Location = new System.Drawing.Point(317, 261);
            this.lb_trustusers.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lb_trustusers.Name = "lb_trustusers";
            this.lb_trustusers.Size = new System.Drawing.Size(295, 164);
            this.lb_trustusers.TabIndex = 8;
            // 
            // tb_user
            // 
            this.tb_user.Location = new System.Drawing.Point(317, 204);
            this.tb_user.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_user.Name = "tb_user";
            this.tb_user.Size = new System.Drawing.Size(295, 22);
            this.tb_user.TabIndex = 6;
            // 
            // bt_ajouter
            // 
            this.bt_ajouter.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bt_ajouter.Location = new System.Drawing.Point(634, 201);
            this.bt_ajouter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_ajouter.Name = "bt_ajouter";
            this.bt_ajouter.Size = new System.Drawing.Size(82, 25);
            this.bt_ajouter.TabIndex = 7;
            this.bt_ajouter.Text = "Ajouter";
            this.bt_ajouter.UseVisualStyleBackColor = false;
            this.bt_ajouter.Click += new System.EventHandler(this.bt_ajouter_Click);
            // 
            // bt_creer
            // 
            this.bt_creer.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bt_creer.Location = new System.Drawing.Point(392, 446);
            this.bt_creer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_creer.Name = "bt_creer";
            this.bt_creer.Size = new System.Drawing.Size(121, 30);
            this.bt_creer.TabIndex = 9;
            this.bt_creer.Text = "Créer";
            this.bt_creer.UseVisualStyleBackColor = false;
            this.bt_creer.Click += new System.EventHandler(this.bt_creer_Click);
            // 
            // bt_back
            // 
            this.bt_back.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bt_back.Location = new System.Drawing.Point(41, 446);
            this.bt_back.Margin = new System.Windows.Forms.Padding(4);
            this.bt_back.Name = "bt_back";
            this.bt_back.Size = new System.Drawing.Size(142, 30);
            this.bt_back.TabIndex = 10;
            this.bt_back.Text = "Revenir";
            this.bt_back.UseVisualStyleBackColor = false;
            this.bt_back.Click += new System.EventHandler(this.bt_back_Click);
            // 
            // cb_sh_pwd
            // 
            this.cb_sh_pwd.AutoSize = true;
            this.cb_sh_pwd.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.cb_sh_pwd.Location = new System.Drawing.Point(648, 117);
            this.cb_sh_pwd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_sh_pwd.Name = "cb_sh_pwd";
            this.cb_sh_pwd.Size = new System.Drawing.Size(158, 20);
            this.cb_sh_pwd.TabIndex = 23;
            this.cb_sh_pwd.Text = "Afficher mot de passe";
            this.cb_sh_pwd.UseVisualStyleBackColor = true;
            this.cb_sh_pwd.CheckedChanged += new System.EventHandler(this.cb_sh_pwd_CheckedChanged);
            // 
            // lb_pwd_m
            // 
            this.lb_pwd_m.AutoSize = true;
            this.lb_pwd_m.Location = new System.Drawing.Point(314, 103);
            this.lb_pwd_m.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_pwd_m.Name = "lb_pwd_m";
            this.lb_pwd_m.Size = new System.Drawing.Size(0, 16);
            this.lb_pwd_m.TabIndex = 24;
            // 
            // lb_tu_m
            // 
            this.lb_tu_m.AutoSize = true;
            this.lb_tu_m.Location = new System.Drawing.Point(314, 181);
            this.lb_tu_m.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_tu_m.Name = "lb_tu_m";
            this.lb_tu_m.Size = new System.Drawing.Size(0, 16);
            this.lb_tu_m.TabIndex = 25;
            // 
            // lb_name_m
            // 
            this.lb_name_m.AutoSize = true;
            this.lb_name_m.Location = new System.Drawing.Point(314, 42);
            this.lb_name_m.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_name_m.Name = "lb_name_m";
            this.lb_name_m.Size = new System.Drawing.Size(0, 16);
            this.lb_name_m.TabIndex = 26;
            // 
            // bt_generate
            // 
            this.bt_generate.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bt_generate.Location = new System.Drawing.Point(634, 143);
            this.bt_generate.Margin = new System.Windows.Forms.Padding(4);
            this.bt_generate.Name = "bt_generate";
            this.bt_generate.Size = new System.Drawing.Size(185, 28);
            this.bt_generate.TabIndex = 24;
            this.bt_generate.Text = "Générer un mot de passe";
            this.bt_generate.UseVisualStyleBackColor = false;
            this.bt_generate.Click += new System.EventHandler(this.bt_generate_Click);
            // 
            // pwd_lvl
            // 
            this.pwd_lvl.AutoSize = true;
            this.pwd_lvl.Location = new System.Drawing.Point(314, 155);
            this.pwd_lvl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.pwd_lvl.Name = "pwd_lvl";
            this.pwd_lvl.Size = new System.Drawing.Size(0, 16);
            this.pwd_lvl.TabIndex = 25;
            // 
            // CreationCoffreFort
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(891, 505);
            this.Controls.Add(this.lb_name_m);
            this.Controls.Add(this.lb_tu_m);
            this.Controls.Add(this.lb_pwd_m);
            this.Controls.Add(this.pwd_lvl);
            this.Controls.Add(this.bt_generate);
            this.Controls.Add(this.cb_sh_pwd);
            this.Controls.Add(this.bt_back);
            this.Controls.Add(this.bt_creer);
            this.Controls.Add(this.bt_ajouter);
            this.Controls.Add(this.tb_user);
            this.Controls.Add(this.lb_trustusers);
            this.Controls.Add(this.tb_master_pwd);
            this.Controls.Add(this.tb_name);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CreationCoffreFort";
            this.Text = "CreationCoffreFort";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CreationCoffreFort_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.TextBox tb_master_pwd;
        private System.Windows.Forms.ListBox lb_trustusers;
        private System.Windows.Forms.TextBox tb_user;
        private System.Windows.Forms.Button bt_ajouter;
        private System.Windows.Forms.Button bt_creer;
        private System.Windows.Forms.Button bt_back;
        private System.Windows.Forms.CheckBox cb_sh_pwd;
        private System.Windows.Forms.Label lb_pwd_m;
        private System.Windows.Forms.Label lb_tu_m;
        private System.Windows.Forms.Label lb_name_m;
        private System.Windows.Forms.Button bt_generate;
        private System.Windows.Forms.Label pwd_lvl;
    }
}