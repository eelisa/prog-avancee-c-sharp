﻿namespace CoffreFort
{
    partial class AjoutTrustUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_tu = new System.Windows.Forms.ListBox();
            this.tb_user = new System.Windows.Forms.TextBox();
            this.bt_add = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bt_val = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lb_tu
            // 
            this.lb_tu.FormattingEnabled = true;
            this.lb_tu.ItemHeight = 16;
            this.lb_tu.Location = new System.Drawing.Point(271, 109);
            this.lb_tu.Name = "lb_tu";
            this.lb_tu.Size = new System.Drawing.Size(244, 164);
            this.lb_tu.TabIndex = 0;
            // 
            // tb_user
            // 
            this.tb_user.Location = new System.Drawing.Point(271, 33);
            this.tb_user.Name = "tb_user";
            this.tb_user.Size = new System.Drawing.Size(244, 22);
            this.tb_user.TabIndex = 1;
            // 
            // bt_add
            // 
            this.bt_add.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bt_add.Location = new System.Drawing.Point(406, 61);
            this.bt_add.Name = "bt_add";
            this.bt_add.Size = new System.Drawing.Size(109, 28);
            this.bt_add.TabIndex = 2;
            this.bt_add.Text = "Ajouter";
            this.bt_add.UseVisualStyleBackColor = false;
            this.bt_add.Click += new System.EventHandler(this.bt_add_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(24, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Utilisateur de confiance :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(24, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(213, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Liste des utilisateurs de confiance :";
            // 
            // bt_val
            // 
            this.bt_val.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bt_val.Location = new System.Drawing.Point(406, 279);
            this.bt_val.Name = "bt_val";
            this.bt_val.Size = new System.Drawing.Size(109, 23);
            this.bt_val.TabIndex = 5;
            this.bt_val.Text = "Valider";
            this.bt_val.UseVisualStyleBackColor = false;
            this.bt_val.Click += new System.EventHandler(this.bt_val_Click);
            // 
            // AjoutTrustUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(603, 326);
            this.Controls.Add(this.bt_val);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bt_add);
            this.Controls.Add(this.tb_user);
            this.Controls.Add(this.lb_tu);
            this.Name = "AjoutTrustUser";
            this.Text = "AjoutTrustUser";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lb_tu;
        private System.Windows.Forms.TextBox tb_user;
        private System.Windows.Forms.Button bt_add;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bt_val;
    }
}