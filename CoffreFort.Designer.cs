﻿namespace CoffreFort
{
    partial class CoffreFort
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CoffreFort));
            this.bt_open = new System.Windows.Forms.Button();
            this.bt_create = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bt_open
            // 
            this.bt_open.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.bt_open.Location = new System.Drawing.Point(322, 114);
            this.bt_open.Name = "bt_open";
            this.bt_open.Size = new System.Drawing.Size(163, 29);
            this.bt_open.TabIndex = 0;
            this.bt_open.Text = "Ouvrir un coffre-fort";
            this.bt_open.UseVisualStyleBackColor = false;
            this.bt_open.Click += new System.EventHandler(this.bt_open_Click);
            // 
            // bt_create
            // 
            this.bt_create.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.bt_create.Location = new System.Drawing.Point(322, 160);
            this.bt_create.Name = "bt_create";
            this.bt_create.Size = new System.Drawing.Size(163, 29);
            this.bt_create.TabIndex = 1;
            this.bt_create.Text = "Créer un coffre-fort";
            this.bt_create.UseVisualStyleBackColor = false;
            this.bt_create.Click += new System.EventHandler(this.bt_create_Click);
            // 
            // CoffreFort
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(535, 314);
            this.Controls.Add(this.bt_create);
            this.Controls.Add(this.bt_open);
            this.Name = "CoffreFort";
            this.Text = "CoffreFort";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CoffreFort_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bt_open;
        private System.Windows.Forms.Button bt_create;
    }
}