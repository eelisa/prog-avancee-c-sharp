﻿namespace CoffreFort
{
    partial class Ouverture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ouverture));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_trustu = new System.Windows.Forms.TextBox();
            this.tb_mpwd = new System.Windows.Forms.TextBox();
            this.bt_valider = new System.Windows.Forms.Button();
            this.lb_error_pwd = new System.Windows.Forms.Label();
            this.lb_error_tu = new System.Windows.Forms.Label();
            this.bt_back = new System.Windows.Forms.Button();
            this.cb_pwd = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(33, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nom d\'utilisateur";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(33, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mot de passe maître";
            // 
            // tb_trustu
            // 
            this.tb_trustu.Location = new System.Drawing.Point(189, 51);
            this.tb_trustu.Name = "tb_trustu";
            this.tb_trustu.Size = new System.Drawing.Size(204, 22);
            this.tb_trustu.TabIndex = 2;
            // 
            // tb_mpwd
            // 
            this.tb_mpwd.Location = new System.Drawing.Point(189, 101);
            this.tb_mpwd.Name = "tb_mpwd";
            this.tb_mpwd.PasswordChar = '•';
            this.tb_mpwd.Size = new System.Drawing.Size(204, 22);
            this.tb_mpwd.TabIndex = 3;
            // 
            // bt_valider
            // 
            this.bt_valider.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bt_valider.Location = new System.Drawing.Point(189, 160);
            this.bt_valider.Name = "bt_valider";
            this.bt_valider.Size = new System.Drawing.Size(202, 23);
            this.bt_valider.TabIndex = 4;
            this.bt_valider.Text = "Valider";
            this.bt_valider.UseVisualStyleBackColor = false;
            this.bt_valider.Click += new System.EventHandler(this.bt_valider_Click);
            // 
            // lb_error_pwd
            // 
            this.lb_error_pwd.AutoSize = true;
            this.lb_error_pwd.Location = new System.Drawing.Point(186, 126);
            this.lb_error_pwd.Name = "lb_error_pwd";
            this.lb_error_pwd.Size = new System.Drawing.Size(0, 16);
            this.lb_error_pwd.TabIndex = 5;
            // 
            // lb_error_tu
            // 
            this.lb_error_tu.AutoSize = true;
            this.lb_error_tu.Location = new System.Drawing.Point(186, 76);
            this.lb_error_tu.Name = "lb_error_tu";
            this.lb_error_tu.Size = new System.Drawing.Size(0, 16);
            this.lb_error_tu.TabIndex = 7;
            // 
            // bt_back
            // 
            this.bt_back.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bt_back.Location = new System.Drawing.Point(27, 369);
            this.bt_back.Name = "bt_back";
            this.bt_back.Size = new System.Drawing.Size(95, 30);
            this.bt_back.TabIndex = 8;
            this.bt_back.Text = "Revenir";
            this.bt_back.UseVisualStyleBackColor = false;
            this.bt_back.Click += new System.EventHandler(this.bt_back_Click);
            // 
            // cb_pwd
            // 
            this.cb_pwd.AutoSize = true;
            this.cb_pwd.BackColor = System.Drawing.Color.Transparent;
            this.cb_pwd.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.cb_pwd.Location = new System.Drawing.Point(428, 100);
            this.cb_pwd.Name = "cb_pwd";
            this.cb_pwd.Size = new System.Drawing.Size(172, 20);
            this.cb_pwd.TabIndex = 9;
            this.cb_pwd.Text = "Afficher le mot de passe";
            this.cb_pwd.UseVisualStyleBackColor = false;
            this.cb_pwd.CheckedChanged += new System.EventHandler(this.cb_pwd_CheckedChanged);
            // 
            // Ouverture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(730, 422);
            this.Controls.Add(this.cb_pwd);
            this.Controls.Add(this.bt_back);
            this.Controls.Add(this.lb_error_tu);
            this.Controls.Add(this.lb_error_pwd);
            this.Controls.Add(this.bt_valider);
            this.Controls.Add(this.tb_mpwd);
            this.Controls.Add(this.tb_trustu);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Ouverture";
            this.Text = "Ouverture";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Ouverture_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_trustu;
        private System.Windows.Forms.TextBox tb_mpwd;
        private System.Windows.Forms.Button bt_valider;
        public System.Windows.Forms.Label lb_error_pwd;
        public System.Windows.Forms.Label lb_error_tu;
        private System.Windows.Forms.Button bt_back;
        private System.Windows.Forms.CheckBox cb_pwd;
    }
}