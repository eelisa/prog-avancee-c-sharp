﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace CoffreFort
{
    internal partial class EditionMotDePasse : Form
    {
        // Attributs de la classe EditionMotDePasse
        public CoffreFortOuvert cf_open;
        public int index;

        // Constructeur
        public EditionMotDePasse(CoffreFortOuvert cfo, int ind)
        {
            InitializeComponent();
            cf_open = cfo;
            index = ind;
        }

        // Fermetude de la fenêtre
        private void EditionMotDePasse_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
        }

        // Procédure au clic sur le bouton Éditer
        private void bt_edit_pwd_Click(object sender, EventArgs e)
        {
            cf_open.dgv_pwdarray.Rows[this.index].Cells[0].Value = tb_title.Text;
            cf_open.dgv_pwdarray.Rows[this.index].Cells[1].Value = tb_email.Text;
            cf_open.dgv_pwdarray.Rows[this.index].Cells[2].Value = tb_login.Text;
            cf_open.dgv_pwdarray.Rows[this.index].Cells[3].Value = tb_url.Text;
            cf_open.dgv_pwdarray.Rows[this.index].Cells[4].Value = "••••••••••";
            this.Hide();

        }

        // Procédure pour afficher le mot de passe en clair
        private void cb_sh_pwd_CheckedChanged(object sender, EventArgs e)
        {
            tb_pwd.PasswordChar = cb_sh_pwd.Checked ? '\0' : '•';
        }

        // Génération d'un mot de passe fort à 16 caractères
        private void bt_generate_Click(object sender, EventArgs e)
        {
            const string cs = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLM?OPQRSTUVWXYZ&(_@/),;.?][#*$^%";

            StringBuilder sb = new StringBuilder();
            Random rand = new Random();
            for (int i = 0; i < 16; i++)
            {
                int index = rand.Next(cs.Length);
                sb.Append(cs[index]);

            }

            tb_pwd.Text = sb.ToString();
        }

        // Vérification politique de mot de passe
        private void tb_pwd_TextChanged(object sender, EventArgs e)
        {
            string password = tb_pwd.Text;

            if (string.IsNullOrEmpty(password))
                pwd_lvl.Text = "";

            if (password.Length >= 1 && password.Length < 4)
            {
                pwd_lvl.ForeColor = System.Drawing.Color.Red;
                pwd_lvl.Text = "Mot de passe très faible";
            }

            if (password.Length >= 8)
            {
                pwd_lvl.ForeColor = System.Drawing.Color.Orange;
                pwd_lvl.Text = "Mot de passe moyen";
            }

            if (password.Length >= 12)
            {
                if (Regex.IsMatch(password, @"[0-9]+(\.[0-9][0-9]?)?", RegexOptions.ECMAScript))   //contient que des chiffres
                {
                    pwd_lvl.ForeColor = System.Drawing.Color.Orange;
                    pwd_lvl.Text = "Mot de passe moyen";
                }
                if (Regex.IsMatch(password, @"^(?=.*[a-z])(?=.*[A-Z]).+$", RegexOptions.ECMAScript)) //Majuscule et minuscule
                {
                    pwd_lvl.ForeColor = System.Drawing.Color.LimeGreen;
                    pwd_lvl.Text = "Mot de passe fort";
                }
                if (Regex.IsMatch(password, @"[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]", RegexOptions.ECMAScript)) //Contient les caractères spéciaux 
                {
                    pwd_lvl.ForeColor = System.Drawing.Color.Green;
                    pwd_lvl.Text = "Mot de passe trop fort";
                }
            }

        }
    }
}
